#include "OneButton.h"
#include <HID-Project.h>
#include <HID-Settings.h>

#define CENTER 5
#define UP 6
#define DOWN 3
#define LEFT 4
#define RIGHT 2

#define DEBUG_
#define TRACE_

#ifdef DEBUG
#define debug(x) Serial.print(x)
#define debugln(x) Serial.println(x)
#else
#define debug(x) 
#define debugln(x) 
#endif
#ifdef TRACE
#define trace(x) Serial.print(x)
#define traceln(x) Serial.println(x)
#else
#define trace(x) 
#define traceln(x) 
#endif

void notify(int n) {
  delay(2000);
  for(int i=0; i<n; i++) {
    TXLED1;
    delay(250);
    TXLED0;
    delay(250);
  }
  delay(2000);
}

void setup() {
  notify(5);
  RXLED0;
  Serial.begin(9600);
  Keyboard.releaseAll();
  Consumer.releaseAll();
  buttonInit();
}


void loop() {
  buttonProcess();
//  RXLED0;
//  delay(1000); // wait for a second
//  RXLED1;
//  delay(1000); // wait for a second
}

void buttonInit() {
  centerInit();
  upInit();
  downInit();
  leftInit();
  rightInit();
}

void buttonProcess() {
  centerProcess();
  upProcess();
  downProcess();
  leftProcess();
  rightProcess();
}


OneButton center(CENTER, true);

void centerInit() {
  center.attachClick(centerClick);
  center.attachDoubleClick(centerDoubleClick);
  center.attachLongPressStart(centerLongPressStart);
  center.attachLongPressStop(centerLongPressStop);
  center.attachDuringLongPress(centerDuringLongPress);
}

void centerProcess() {
  center.tick();
}

void centerClick() {
  debug("centerClick\n");
  Consumer.press(MEDIA_PLAY_PAUSE);
  delay(20);
  Consumer.release(MEDIA_PLAY_PAUSE);
}

void centerDoubleClick() {
  debug("centerDoubleClick\n");
}

void centerLongPressStart() {
  debug("centerLongStart\n");
}

void centerLongPressStop() {
  debug("\ncenterLongStop\n");
}

void centerDuringLongPress() {
  debug(".");
}

OneButton up(UP, true);

void upInit() {
  up.attachClick(upClick);
  up.attachDoubleClick(upDoubleClick);
  up.attachLongPressStart(upLongPressStart);
  up.attachLongPressStop(upLongPressStop);
  up.attachDuringLongPress(upDuringLongPress);
}

void upProcess() {
  up.tick();
}

void upClick() {
  debug("upClick\n");
}

void upDoubleClick() {
  debug("upDoubleClick\n");
}

void upLongPressStart() {
  debug("upLongStart\n");
}

void upLongPressStop() {
  debug("\nupLongStop\n");
}

void upDuringLongPress() {
  debug("^");
}

OneButton down(DOWN, true);

void downInit() {
  down.attachClick(downClick);
  down.attachDoubleClick(downDoubleClick);
  down.attachLongPressStart(downLongPressStart);
  down.attachLongPressStop(downLongPressStop);
  down.attachDuringLongPress(downDuringLongPress);
}

void downProcess() {
  down.tick();
}

void downClick() {
  debug("downClick\n");
}

void downDoubleClick() {
  debug("downDoubleClick\n");
}

void downLongPressStart() {
  debug("downLongStart\n");
}

void downLongPressStop() {
  debug("\ndownLongStop\n");
}

void downDuringLongPress() {
  debug("v");
}

OneButton left(LEFT, true);

void leftInit() {
  left.attachClick(leftClick);
  left.attachDoubleClick(leftDoubleClick);
  left.attachLongPressStart(leftLongPressStart);
  left.attachLongPressStop(leftLongPressStop);
  left.attachDuringLongPress(leftDuringLongPress);
}

void leftProcess() {
  left.tick();
}

void leftClick() {
  debug("leftClick\n");
  Consumer.press(MEDIA_NEXT);
  delay(20);
  Consumer.release(MEDIA_NEXT);
}

void leftDoubleClick() {
  debug("leftDoubleClick\n");
}

void leftLongPressStart() {
  debug("leftLongStart\n");
}

void leftLongPressStop() {
  debug("\nleftLongStop\n");
}

void leftDuringLongPress() {
  debug("<");
}

OneButton right(RIGHT, true);

void rightInit() {
  right.attachClick(rightClick);
  right.attachDoubleClick(rightDoubleClick);
  right.attachLongPressStart(rightLongPressStart);
  right.attachLongPressStop(rightLongPressStop);
  right.attachDuringLongPress(rightDuringLongPress);
}

void rightProcess() {
  right.tick();
}

void rightClick() {
  debug("rightClick\n");
  Consumer.press(MEDIA_PREV);
  delay(20);
  Consumer.release(MEDIA_PREV);
}

void rightDoubleClick() {
  debug("rightDoubleClick\n");
}

void rightLongPressStart() {
  debug("rightLongStart\n");
}

void rightLongPressStop() {
  debug("\nrightLongStop\n");
}

void rightDuringLongPress() {
  debug(">");
}

